import React from 'react';
import './styles.css';

import FlatList from 'flatlist-react'


export default function Servicos({ history }) {

    function banhoSelected(){

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)
        let quantidade = parseInt(Object.keys(dadosObject).length)
        console.log(quantidade)

        let verificacao = dadosObject[quantidade-1].servicos.includes('banho')

        if(verificacao == true){
            document.getElementById('banho').style.background = 'lightyellow'
            document.getElementById('banho').style.color = '#494949'

            let local = dadosObject[quantidade-1].servicos.indexOf('banho')

            dadosObject[quantidade-1].servicos.splice(local)

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)

        }
        else{
            document.getElementById('banho').style.background = 'lightgreen'
            document.getElementById('banho').style.color = 'white'

            dadosObject[quantidade-1].servicos.push('banho')

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)
        }
        
    }

    function corteDePeloSelected(){

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)
        let quantidade = parseInt(Object.keys(dadosObject).length)

        let verificacao = dadosObject[quantidade-1].servicos.includes('corte')

        if(verificacao == true){
            document.getElementById('corte').style.background = 'lightyellow'
            document.getElementById('corte').style.color = '#494949'

            let local = dadosObject[quantidade-1].servicos.indexOf('corte')

            dadosObject[quantidade-1].servicos.splice(local)

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)

        }
        else{
            document.getElementById('corte').style.background = 'lightgreen'
            document.getElementById('corte').style.color = 'white'

            dadosObject[quantidade-1].servicos.push('corte')

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)
        }
        
    }

    function corteDeUnhaSelected(){

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)
        let quantidade = parseInt(Object.keys(dadosObject).length)

        let verificacao = dadosObject[quantidade-1].servicos.includes('unha')

        if(verificacao == true){
            document.getElementById('unha').style.background = 'lightyellow'
            document.getElementById('unha').style.color = '#494949'

            let local = dadosObject[quantidade-1].servicos.indexOf('unha')

            dadosObject[quantidade-1].servicos.splice(local)

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)

        }
        else{
            document.getElementById('unha').style.background = 'lightgreen'
            document.getElementById('unha').style.color = 'white'

            dadosObject[quantidade-1].servicos.push('unha')

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)
        }
        
    }

    function vacinaSelected(){

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)
        let quantidade = parseInt(Object.keys(dadosObject).length)

        let verificacao = dadosObject[quantidade-1].servicos.includes('vacina')

        if(verificacao == true){
            document.getElementById('vacina').style.background = 'lightyellow'
            document.getElementById('vacina').style.color = '#494949'

            let local = dadosObject[quantidade-1].servicos.indexOf('vacina')

            dadosObject[quantidade-1].servicos.splice(local)

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)

        }
        else{
            document.getElementById('vacina').style.background = 'lightgreen'
            document.getElementById('vacina').style.color = 'white'

            dadosObject[quantidade-1].servicos.push('vacina')

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)
        }
        
    }

    function castrarSelected(){

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)
        let quantidade = parseInt(Object.keys(dadosObject).length)

        let verificacao = dadosObject[quantidade-1].servicos.includes('castrar')

        if(verificacao == true){
            document.getElementById('castrar').style.background = 'lightyellow'
            document.getElementById('castrar').style.color = '#494949'

            let local = dadosObject[quantidade-1].servicos.indexOf('castrar')

            dadosObject[quantidade-1].servicos.splice(local)

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)

        }
        else{
            document.getElementById('castrar').style.background = 'lightgreen'
            document.getElementById('castrar').style.color = 'white'

            dadosObject[quantidade-1].servicos.push('castrar')

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)
        }
        
    }

    function adestrarSelected(){

        let dados = localStorage.getItem('pet')
        let dadosObject = JSON.parse(dados)
        let quantidade = parseInt(Object.keys(dadosObject).length)


        let verificacao = dadosObject[quantidade-1].servicos.includes('adestrar')

        if(verificacao == true){
            document.getElementById('adestrar').style.background = 'lightyellow'
            document.getElementById('adestrar').style.color = '#494949'

            let local = dadosObject[quantidade-1].servicos.indexOf('adestrar')

            dadosObject[quantidade-1].servicos.splice(local)

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)

        }
        else{
            document.getElementById('adestrar').style.background = 'lightgreen'
            document.getElementById('adestrar').style.color = 'white'

            dadosObject[quantidade-1].servicos.push('adestrar')

            let dadosString = JSON.stringify(dadosObject)
            localStorage.setItem('pet',dadosString)
        }
        
    }
      
    function adicionar(){
        window.location.reload()
    }

    function confirmar(){
        history.push('/lista');
    }



    return(
        
        <div className="geralServiços">
            <div className="retanguloServicos" id="retanguloServicos">
                <span>Quais procedimentos<br></br>seu Pet nescessita?</span>
                <div className="servicosRowUm">
                    <input id = "banho" className="banho" type='submit' value='Banho' onClick={()=>banhoSelected()}></input>
                    <input id = "corte" className="corte" type='submit' value='Corte de pelo' onClick={()=>corteDePeloSelected()}></input>
                </div>
                <div className="servicosRowDois">
                    <input id = "unha" className="unha" type='submit' value='Corte de unha' onClick={()=>corteDeUnhaSelected()}></input>
                    <input id = "vacina" className="vacina" type='submit' value='Vacinação' onClick={()=>vacinaSelected()}></input>
                </div>
                <div className="servicosRowTres">
                    <input id = "castrar" className="castrar" type='submit' value='Castrar' onClick={()=>castrarSelected()}></input>
                    <input id = "adestrar" className="adestrar" type='submit' value='Adestrar'onClick={()=>adestrarSelected()} ></input>
                </div>
                <div className="botaoServicos" >
                    <input className="botaoInput" type='submit' value='Adicionar' onClick={()=>adicionar()} ></input>
                </div>

            </div>

            <div className="retanguloLista" id="retanguloLista">
                <span>Lista de serviços selecionados</span>
                <FlatList 
                list={JSON.parse(localStorage.getItem('pet'))[Object.keys(JSON.parse(localStorage.getItem('pet'))).length - 1].servicos}
                renderItem={item =>
                <div className="item">
                    <span>{item}</span>
                </div>}/>
                <div className="botaoLista" >
                    <input className="botaoInput" type='submit' value='Confirmar' onClick={()=>confirmar()}></input>
                </div>
            </div>
        </div>

    )
    }