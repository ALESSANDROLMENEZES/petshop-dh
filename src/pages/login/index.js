import React from 'react';
import './styles.css';
import icone from '../../assets/imgs/16946.jpg'
import cachorro from '../../assets/imgs/cachorro.jpg'

export default function Login({ history }) {


      function inicio (){ // funcao que recebe o nome do usuario e o nome do pet

        let nomeDoDonoDoPet = document.getElementById('nome').value
        let nomeDoPet = document.getElementById('nomeDoPet').value

        localStorage.setItem('dono',nomeDoDonoDoPet)
        
        let dadosPet = [{dono: nomeDoDonoDoPet,
            nome: nomeDoPet,
            tipo: '',
            raca: '',
            idade: '',
            genero: '',
            vacinado:'Não',
            servicos: [],
        }]
            
        let dadosPetString = JSON.stringify(dadosPet)
        localStorage.setItem('pet',dadosPetString) 
        localStorage.setItem('quantidade', 0)      
        history.push('/menu');

    }

    return(
        
        <div className="geralLogin">
            <div className="retangulo">
                <div className="geralIcone">
                    <img className='icone' src={icone}></img>
                </div>

                <div className="nome">
                    <span className="nomeText">Nome:</span>
                    <input id='nome' type='text' placeholder='Qual o seu nome?'></input>
                </div>

                <div className="animal">
                    <span>Nome do Pet:</span>
                    <input id='nomeDoPet' type='text' placeholder='Qual o nome do seu pet?'></input>
                </div>

                <div className="botao" >
                    <input className="botaoInput" type='submit' value='Selecionar serviços' onClick={()=>inicio()}></input>
                </div>

            </div>

            <img className='cachorroImagem' src={cachorro}></img>

        </div>
        )
    }